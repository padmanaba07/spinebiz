<?php

namespace App\Http\Controllers;
use App\Models\Provider;
use Config;

use Illuminate\Http\Request;


class ProviderController extends Controller
{
    public function registerProvider(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'email'=>'required|email|unique:providers',
            'password'=>'required|min:8',
            'client_secret' => 'required',
        ]);
        $provider= Provider::create([
            'name' =>$request->name,
            'email'=>$request->email,
            'password'=>bcrypt($request->password),
            'token' => '',
        ]);
        Config::set('auth.providers.users.model', 'App\Models\Provider');
        $access_token_example = $provider->createToken($request->client_secret)->accessToken;
        //return the access token we generated in the above step
        return response()->json(['token'=>$access_token_example],200);
    }

    public function loginProvider(Request $request)
    {
        $this->validate($request,[
            'email'=>'required|email',
            'password'=>'required|min:8',
            'client_secret' => 'required'
        ]);
        $login_credentials=[
            'email'=>$request->email,
            'password'=>$request->password,
        ];

        Config::set('auth.providers.users.model', 'App\Models\Provider');
        if(auth()->attempt($login_credentials)){
            //generate the token for the user
            $user_login_token= auth()->user()->createToken($request->client_secret)->accessToken;
            //now return this token on success login attempt
            return response()->json(['token' => $user_login_token], 200);
        }
        else{
            //wrong login credentials, return, user not authorised to our system, return error code 401
            return response()->json(['error' => 'UnAuthorised Access'], 401);
        }
    }


    public function userDetails(){
        //returns details
        Config::set('auth.providers.users.model', 'App\Models\Provider');
        return response()->json(['data' => auth()->user()], 200);
    }
}
